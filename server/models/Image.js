const mongoose = require('mongoose');

const ImageSchema = new mongoose.Schema({
    imageName: {
        type: String,
        required: true,
    },
    imageUrl: {
        type: String,
        required: false,
    },
    description: {
        type: String,
        required: false,
    },
});

const Image = mongoose.model('Image', ImageSchema, 'images');

module.exports = Image;

