const express = require('express');
const index = express();
require('dotenv').config();

index.use(express.json());

const connectDB = require('./database');

connectDB()
    .then(() => console.log('Database connected successfully!'))
    .catch((err) => {
        console.error('Database connection failed:', err.message);
        process.exit(1);
    });



index.get('/', (req, res) => {
    res.send('Hello World!');
});

const Image = require('./models/Image');
const uploadImageToS3 = require("./s3Storage");
const deleteImageFromS3 = require("./s3Storage");
const getImageFromS3 = require("./s3Storage");
const getAllImagesFromS3 = require("./s3Storage");

index.get('/images', async (req, res) => {
    try {
        const images = await getAllImagesFromS3();

        res.status(200).json(images);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

index.get('/images/:id', async (req, res) => {
    try {
        const image = await Image.findById(req.params.id);

        if (!image) {
            return res.status(404).json({ message: 'Image not found' });
        }

        const imageData = await getImageFromS3(image.imageUrl);

        // Повернення зображення у відповіді
        res.set('Content-Type', 'image/jpeg');
        res.send(imageData);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

index.post('/upload', async (req, res) => {
    try {
        const { imageName, description } = req.body;
        const file = req.file;

        const imageUrl = await uploadImageToS3(file);

        const newImage = new Image({
            imageName,
            imageUrl,
            description
        });
        await newImage.save();

        res.status(201).json(newImage);
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
});

index.delete('/images/:id', async (req, res) => {
    try {
        const image = await Image.findByIdAndDelete(req.params.id);

        if (!image) {
            return res.status(404).json({ message: 'Image not found' });
        }

        await deleteImageFromS3(image.imageUrl);

        res.status(200).json({ message: 'Image deleted successfully', image });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});



const PORT = process.env.PORT || 3000;
index.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});
